export function setIsAutoLogin
  (loginState, userid, password) {
      // 存储用户名和密码到安卓/ios
      if (window.AndroidWebView) {
        window.JS.setIsAutoLogin(loginState, userid, password);
      } else if (window.WebViewJavascriptBridge) {
        WebViewJavascriptBridge.callHandler("setIsAutoLogin", {
          key1: loginState,
          key2: userid,
          key3: password
        });
      }
    }

    